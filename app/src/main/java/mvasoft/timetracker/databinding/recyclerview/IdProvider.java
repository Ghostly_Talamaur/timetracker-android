package mvasoft.timetracker.databinding.recyclerview;

public interface IdProvider {
    long getId();
}
